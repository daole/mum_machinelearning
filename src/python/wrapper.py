import json
import sys
import os.path
import numpy as np
import ast
import gensim
import re
from gensim.models import Doc2Vec
from collections import OrderedDict
from collections import defaultdict
from gensim.parsing.preprocessing import preprocess_string
from gensim.parsing.preprocessing import strip_tags
from gensim.parsing.preprocessing import strip_punctuation
from gensim.parsing.preprocessing import strip_multiple_whitespaces
from gensim.parsing.preprocessing import strip_numeric
from gensim.parsing.preprocessing import remove_stopwords

def normalize_text(text):
    # Lower characters
    text = text.lower()

    # Replace abbreviated words
    text = re.sub(r"i'm", "i am", text)
    text = re.sub(r"he's", "he is", text)
    text = re.sub(r"she's", "she is", text)
    text = re.sub(r"it's", "it is", text)
    text = re.sub(r"that's", "that is", text)
    text = re.sub(r"what's", "that is", text)
    text = re.sub(r"where's", "where is", text)
    text = re.sub(r"how's", "how is", text)
    text = re.sub(r"\'ll", " will", text)
    text = re.sub(r"\'ve", " have", text)
    text = re.sub(r"\'re", " are", text)
    text = re.sub(r"\'d", " would", text)
    text = re.sub(r"\'re", " are", text)
    text = re.sub(r"won't", "will not", text)
    text = re.sub(r"can't", "cannot", text)
    text = re.sub(r"n't", " not", text)
    text = re.sub(r"n'", "ng", text)
    text = re.sub(r"'bout", "about", text)
    text = re.sub(r"'til", "until", text)

    # Remove stop words
    text = remove_stopwords(text)

    # Do some more text cleaning work
    filterString = [lambda x: x.lower(), strip_tags, strip_punctuation, strip_multiple_whitespaces, strip_numeric]
    return preprocess_string(text, filterString)

def useModel(productId, message):
    # Preprocess the message and get back the word tokens of the message
    tokenQuestion = normalize_text(message)
    length = len(tokenQuestion)

    # If the length of input quesion is too short (the description of a problem or the context is not clear),
    # append the word 'dummy' many times to reduce the similarity score of the trained quesion found by the model,
    # so that if the score is low enough the system can reply to the user that it does not understand the question
    if length < 3:
        for i in range((3 - length) * 15):
            tokenQuestion.append('dummy')

    # Add the product id several times to the input question so that the product id becomes a keyword,
    # which gives higher similarity score to the trained questions with same product id.
    # Here, we are trying to let the model know which product we are asking about
    # so that it more likely picks the answer of the trained question which is most similar to the input question
    for i in range(min(10, length * 2)):
        tokenQuestion.append(productId)

    # Get a vector representing the input message
    inferred_docvec = loadedModel.infer_vector(tokenQuestion)

    # Get the vectors that are most similar to the input message vector
    test = (loadedModel, loadedModel.docvecs.most_similar([inferred_docvec], topn=10))

    # Return result in JSON format
    listDict = []
    for item in test[1]:
        newDict = {"index" : int(item[0]), "score" : float(item[1])}
        listDict.append(newDict)
    return json.dumps(listDict)

# Load pretrained model
loadedModel = Doc2Vec.load(os.path.join(os.path.dirname(__file__), "model.model"))

# Capture selected product id and input message sent from NodeJs server process
productId = sys.argv[1]
message = sys.argv[2]

# Use model to retrive the most similar questions in our dataset
result = useModel(productId, message)

# Return result to NodeJs
print(result)
sys.stdout.flush()