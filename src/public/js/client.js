// Initialize angular module
var module = angular.module('project', []);

// Initialize web socket
var socket = io.connect('/');

// Initialize main angular controller of this modile
module.controller('projectController', ['$scope', function($scope) {
    var messagesHtmlElement = $('#messages');
    $scope.productIds = [];
    $scope.messages = [];

    // Get random product list from server and show them on a select box
    socket.emit('productIds', null, function(productIds) {
        $scope.$apply(function() {
            $scope.productIds = productIds;
        });
    });
    
    /*
        Send message that user input to server along with the selected product
        if the message is not empty and a product is selected.
        Also wait for the response message from the server and apply it to message array.
    */
    $scope.sendMessage = function() {
        // Check if the input message is empty and a product is selected
        if($scope.productId && $scope.content) {
            content = $scope.content;

            // Clear the html input element
            $scope.content = '';

            // Append the message to message array, indicating that the message is of user
            appendMessage('you', content);

            // Send message to server with the product id
            socket.emit('message', {
                productId: $scope.productId,
                content: content
            }, function(content) {
                // Append server's response message to message array
                $scope.$apply(function() {
                    appendMessage('machine', content);
                });
            });
        }
    };

    /*
        Append message to the message array so that it can be shown on UI,
        Also scroll to the bottom of the div element because the new message will appear there
     */
    function appendMessage(from, content) {
        // Append message to the message array
        $scope.messages.push({
            from: from,
            content: content
        });

        // Scroll to the bottom of the div element that shows messages
        setTimeout(function() {
            messagesHtmlElement.scrollTop(messagesHtmlElement.prop('scrollHeight'));
        }, 0);
    } 
}]);