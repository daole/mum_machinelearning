var fs = require('fs');
var http = require('http');
var express = require('express');
var socketio = require('socket.io');
var childProcess = require('child_process');
var path = require('path');

// Read dataset
var jsonData = JSON.parse(fs.readFileSync('./python/data.json', 'utf8'));

// Randomly pick 20 products
var productIds = [];
while(productIds.length < 20) {
    var i = getRandomInt(jsonData.length);
    var row = jsonData[i];
    if(productIds.indexOf(row.asin) < 0) {
        productIds.push(row.asin);
    }
}

// Initialize web server and websocket
var app = express();
var server = http.createServer(app);
var socket = socketio(server);
app.use('/public', express.static('./public'));
app.get('/', function (request, response) {
    response.sendFile(path.join(__dirname + '/public/views/pages/index.html'));
});

// Handle new web socket connection
socket.on('connection', function(client) {
    // Return product ids to client
    client.on('productIds', function(data, callback) {
        if(callback) {
            callback(productIds);
        }
    });

    // Handle message posted from user
    client.on('message', function(message, callback) {
        console.log('message', message);

        // Check if message is valid
        if(message.productId && message.content) {
            // Spawn a Python process to handle user's message
            var spawn = childProcess.spawn('python', [
                './python/wrapper.py',
                message.productId,
                message.content
            ]);

            // Receive returned result from Python process
            spawn.stdout.on('data', function (data) {
                var response = data.toString();
                var parsedResponse = JSON.parse(response);
                console.log('parsedResponse', parsedResponse);
                var resultRow = null;

                // Filter out the Python result to get the answer that has matching product id with the one selected by the user
                // and the similarity score must be greater than the threshold
                for(var i = 0; i < parsedResponse.length; i++) {
                    var row = parsedResponse[i];
                    dataRow = jsonData[row.index - 1];
                    if(dataRow.asin === message.productId) {
                        if(row.score >= 0.7) {
                            resultRow = dataRow;
                        }
                        break;
                    }
                }

                // If there is no matching answer, return a default one
                var answer = 'I apologize for not being able to answer this question. I will forward it to another staff.';
                if(resultRow) {
                    answer = resultRow.answer;
                }
                if(callback) {
                    callback(answer);
                }
            });
        }
    });
});

server.listen(8080);

process.on('uncaughtException', function(error) {
    console.log(error);
});

function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}